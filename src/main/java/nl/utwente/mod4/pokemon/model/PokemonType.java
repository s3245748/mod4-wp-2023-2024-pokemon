package nl.utwente.mod4.pokemon.model;

import java.util.ArrayList;
import java.util.List;

public class PokemonType extends NamedEntity {

    public int pokedexNumber;
    public int generation;
    public String japaneseName;
    public String classification;
    public List<String> abilities;
    public float baseHeight;
    public float baseWeight;
    public int baseHp;
    public int baseAttack;
    public int baseSpAttack;
    public int baseDefense;
    public int baseSpDefense;
    public int baseSpeed;
    public int captureRate;
    public boolean isLegendary;
    public String imgUrl;
    public String primaryType;
    public String secondaryType;

    public PokemonType() { //Constructor
        super();
        pokedexNumber = 0;
        generation = 0;
        japaneseName = null;
        classification = null;
        abilities = null;
        baseHeight = 0;
        baseWeight = 0;
        baseHp = 0;
        baseAttack = 0;
        baseSpAttack = 0;
        baseDefense = 0;
        baseSpDefense = 0;
        baseSpeed = 0;
        captureRate = 0;
        isLegendary = false;
        imgUrl = null;
    }

}
